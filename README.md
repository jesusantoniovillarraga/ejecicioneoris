# Neoris Prueba Tecnica

## Movimientos
Este proyecto es una prueba tecnica para Neoris para mostrar my habilidad con java, spring booy y hibernate

## Descripcion
El objetivo es crear un API Rest para administrar movimientos bancarios de clientes

## Requerimientos
Para iniciar la aplicacion se necesita:

- [Open JDK](https://jdk.java.net/archive/)
- [Gradle](https://gradle.org/releases/)
- [Docker para windows](https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe)
- [Docker para mac](https://docs.docker.com/desktop/install/mac-install/)
- [Docker para linux](https://docs.docker.com/engine/install/)


## Instalacion
Use git para clonar este repositorio en su equipo.

con HTTPS
```
git clone https://gitlab.com/jesusantoniovillarraga/ejecicioneoris.git
```

## Uso
Esta aplicaion esta empaquetada como un jar que tiene Tomcat 8 en su interior. No es necesario instalar Tomcat o Jboss. se puede iniciar usando el comando de java -jar

Puede iniciar el proyecto y ejecutar los test ejecutando
```
gradle build
```
Una vez fue construido correctamente puede iniciar el servicio usando
```
gradle bootRun
```
Una vez la aplicacion inicia correctamente deberia ver algo como esto
```
2023-05-09T20:45:49.379-05:00  INFO 13504 --- [  restartedMain] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2023-05-09T20:45:49.386-05:00  INFO 13504 --- [  restartedMain] c.n.e.EjecicioNeorisApplication          : Started EjecicioNeorisApplication in 2.575 seconds (process running for 3.042)
```
***

### Docker
El proyecto puede ser desplegado usando Docker para ello hay que realizar un build del proyecto
```
gradle clean build
```
Una vez la aplicaicon ha sido contruida se puede contruir la imagen de docker usando
```
docker build -t ejercicioneoris .
```
por ultimo se puede ejecutar el contenedor usando el comando
```
docker run -p 8080:8080 ejercicioneoris
```
***
## Sobre el proyecto
El proyecto utiliza una base de datos en memoria H2. Si las propiedades de coneccion a la base de datos, podras llamar algunos endpoint definidos en com.neoris.ejercicioneoris.controller en el puerto 8080

Puede obtener mas informacion sobre los endpoints en [Swagger-UI](http://localhost:8080/swagger-ui/index.html) cuando haya iniciado este proyecto.

Puede conectarse a la base de datos H2 en [H2 Console](http://localhost:8080/h2-console/) cuando inicia este proyecto.

Finalmente puede obtener una coleccion de postman con todos los endpoints y sus parametros dentro de "src/main/resources/Neoris.postman_collection.json"

No olvide importar tambien el entorno de postman que se encuentra dentro de "src/main/resources/Neoris_Enviroment.postman_environment.json"