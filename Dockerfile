# Usa una imagen base con Java 17
FROM openjdk:17

# Copia el archivo JAR de la aplicación en el contenedor
COPY build/libs/EjecicioNeoris-0.0.1-SNAPSHOT.jar /app/app.jar

# Establece el directorio de trabajo dentro del contenedor
WORKDIR /app

ENTRYPOINT ["java","-jar","/app/app.jar","-web -webAllowOthers -tcp -tcpAllowOthers -browser"]

# Expone el puerto en el que se ejecutará la aplicación
EXPOSE 8080

# Comando para ejecutar la aplicación cuando se inicie el contenedor
CMD ["java", "-jar", "app.jar"]