package com.neoris.ejecicioneoris.utils;

import com.neoris.ejecicioneoris.application.dto.movmiento.MovimientoDto;
import com.neoris.ejecicioneoris.domain.enums.TipoMovimiento;

import java.util.LinkedList;
import java.util.List;

public class TestUtils {
    public static MovimientoDto crearMovimientoDto(){
        MovimientoDto movimientoDto = new MovimientoDto();
        movimientoDto.setNumeroCuenta("9099581745679890");
        movimientoDto.setValor(100);
        movimientoDto.setTipoMovimiento(TipoMovimiento.CREDITO.toString());
        return movimientoDto;
    }

    public static List<MovimientoDto> crearListaMovimiento(){
        List<MovimientoDto> lista = new LinkedList<>();
        lista.add(crearMovimientoDto());
        return lista;
    }
}
