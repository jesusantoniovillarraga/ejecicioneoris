package com.neoris.ejecicioneoris.controller;

import com.neoris.ejecicioneoris.application.controllers.MovimientoController;
import com.neoris.ejecicioneoris.application.dto.movmiento.MovimientoDto;
import com.neoris.ejecicioneoris.application.usecases.contracts.cuenta.ActualizarCuentaUseCaseInterface;
import com.neoris.ejecicioneoris.application.usecases.contracts.movimiento.CrearMovimientoUseCaseInterface;
import com.neoris.ejecicioneoris.application.usecases.contracts.movimiento.ObtenerMovimientoUseCaseInterface;
import com.neoris.ejecicioneoris.domain.errors.ApiError;
import com.neoris.ejecicioneoris.utils.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MovimientoControllerTest {

    @Mock
    private CrearMovimientoUseCaseInterface crearMovimientoUseCase;

    @Mock
    private ActualizarCuentaUseCaseInterface actualizarCuentaUseCase;

    @Mock
    private ObtenerMovimientoUseCaseInterface obtenerMovimientoUseCase;

    @InjectMocks
    private MovimientoController controller;

    @Test
    public void testCrearMovimiento() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        MovimientoDto movimientoDto = TestUtils.crearMovimientoDto();

        ResponseEntity result = controller.crearMovimiento(movimientoDto);
        assertEquals(result.getStatusCode(), HttpStatus.CREATED);
        assertEquals(result.getBody(), "Movmiento creado exitosamente");
    }

    @Test
    public void testCrearMovimientoException() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        MovimientoDto movimientoDto = TestUtils.crearMovimientoDto();

        doThrow(new Exception("Saldo no disponible")).when(actualizarCuentaUseCase).actualizarCuenta(movimientoDto.getNumeroCuenta(),movimientoDto);

        ResponseEntity result = controller.crearMovimiento(movimientoDto);
        assertEquals(result.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertEquals(((ApiError)result.getBody()).getMessage(), "Saldo no disponible");
        assertEquals(((ApiError)result.getBody()).getStatus(),  HttpStatus.BAD_REQUEST);
        assertEquals(((ApiError)result.getBody()).getDebugMessage(), "Saldo no disponible");
    }

    @Test
    public void testObtenerMovimientoEntreFechas() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        when(obtenerMovimientoUseCase
                .obtenerMovimientoEntreFechas(LocalDate.now(),LocalDate.now(),"9099581745679890"))
                .thenReturn(TestUtils.crearListaMovimiento());

        ResponseEntity result = controller.obtenerMovimimentosEntreFechas("9099581745679890", LocalDate.now(),LocalDate.now());
        assertEquals(result.getStatusCode(), HttpStatus.OK);
        assertEquals(((List<MovimientoDto>) result.getBody()).get(0).getNumeroCuenta(), TestUtils.crearListaMovimiento().get(0).getNumeroCuenta());
        assertEquals(((List<MovimientoDto>) result.getBody()).get(0).getTipoMovimiento(), TestUtils.crearListaMovimiento().get(0).getTipoMovimiento());
        assertEquals(((List<MovimientoDto>) result.getBody()).get(0).getValor(), TestUtils.crearListaMovimiento().get(0).getValor());
    }

}
