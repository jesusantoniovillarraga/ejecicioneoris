package com.neoris.ejecicioneoris.infrastructure.mappers;

import com.neoris.ejecicioneoris.application.dto.cliente.ClienteDto;
import com.neoris.ejecicioneoris.domain.entities.Cliente;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = { CuentaMapperInterface.class,PersonaMapperInterface.class })
public interface ClienteMapperInterface {

    ClienteMapperInterface MAPPER = Mappers.getMapper(ClienteMapperInterface.class);

    @Mapping(source = "persona", target = "persona")
    @Mapping(source = "estado", target = "estado")
    @Mapping(source = "cuentas", target = "cuentas")
    @Mapping(source = "contrasenia", target = "contrasenia")
    Cliente toCliente(ClienteDto clienteDto);

    @InheritInverseConfiguration
    @Mapping(source = "contrasenia", target = "contrasenia", ignore = true)
    ClienteDto fromCliente(Cliente cliente);
}
