package com.neoris.ejecicioneoris.infrastructure.mappers;

import com.neoris.ejecicioneoris.application.dto.movmiento.MovimientoDto;
import com.neoris.ejecicioneoris.domain.entities.Movimiento;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = { CuentaMapperInterface.class })
public interface MovimientoMapperInterface {

    MovimientoMapperInterface MAPPER = Mappers.getMapper(MovimientoMapperInterface.class);

    @Mapping(source = "tipoMovimiento", target = "tipoMovimiento")
    @Mapping(source = "valor", target = "valor")
    @Mapping(source = "numeroCuenta", target = "cuenta.numeroCuenta")
    Movimiento toMovimiento(MovimientoDto movimientoDto);

    @InheritInverseConfiguration
    MovimientoDto fromMovimiento(Movimiento movimiento);
}
