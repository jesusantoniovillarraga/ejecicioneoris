package com.neoris.ejecicioneoris.infrastructure.mappers;

import com.neoris.ejecicioneoris.application.dto.persona.PersonaDto;
import com.neoris.ejecicioneoris.domain.entities.Persona;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = { ClienteMapperInterface.class })
public interface PersonaMapperInterface {
    PersonaMapperInterface MAPPER = Mappers.getMapper( PersonaMapperInterface.class );

    @Mapping(source = "identificacion", target = "id")
    @Mapping(source = "nombre", target = "nombre")
    @Mapping(source = "genero", target = "genero")
    @Mapping(source = "direccion", target = "direccion")
    @Mapping(source = "telefono", target = "telefono")
    @Mapping(source = "edad", target = "edad")
    Persona toPersona(PersonaDto personaDto);

    @InheritInverseConfiguration
    PersonaDto fromPersona(Persona persona);
}
