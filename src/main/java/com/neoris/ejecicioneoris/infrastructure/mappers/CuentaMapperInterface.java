package com.neoris.ejecicioneoris.infrastructure.mappers;

import com.neoris.ejecicioneoris.application.dto.cuenta.CuentaDto;
import com.neoris.ejecicioneoris.domain.entities.Cuenta;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {CuentaMapperInterface.class, PersonaMapperInterface.class})
public interface CuentaMapperInterface {

    CuentaMapperInterface MAPPER = Mappers.getMapper(CuentaMapperInterface.class);

    @Mapping(source = "numeroCuenta", target = "numeroCuenta")
    @Mapping(source = "tipoCuenta", target = "tipoCuenta")
    @Mapping(source = "saldoInicial", target = "saldoInicial")
    Cuenta toCuenta(CuentaDto cuentaDto);

    @InheritInverseConfiguration
    @Mapping(source = "cliente.persona.id", target = "identificacion")
    CuentaDto fromCuenta(Cuenta cuenta);
}
