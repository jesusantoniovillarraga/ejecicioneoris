package com.neoris.ejecicioneoris.infrastructure.repositories;

import com.neoris.ejecicioneoris.domain.entities.Cuenta;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CuentaRepositoryInterface extends JpaRepository<Cuenta, String> {

    @EntityGraph(attributePaths = {"cliente", "movimientos"})
    Optional<Cuenta> findById(String id);
}
