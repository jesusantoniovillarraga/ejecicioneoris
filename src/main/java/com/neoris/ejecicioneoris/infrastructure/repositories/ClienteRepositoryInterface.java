package com.neoris.ejecicioneoris.infrastructure.repositories;

import com.neoris.ejecicioneoris.domain.entities.Cliente;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClienteRepositoryInterface extends JpaRepository<Cliente, Integer> {

    @EntityGraph(attributePaths = {"persona", "cuentas"})
    Optional<Cliente> findById(Integer id);

    Optional<Cliente> findClienteByPersonaId(String id);
}
