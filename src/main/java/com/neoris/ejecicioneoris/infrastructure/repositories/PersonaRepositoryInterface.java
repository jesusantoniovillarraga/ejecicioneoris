package com.neoris.ejecicioneoris.infrastructure.repositories;

import com.neoris.ejecicioneoris.domain.entities.Cliente;
import com.neoris.ejecicioneoris.domain.entities.Persona;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PersonaRepositoryInterface extends JpaRepository<Persona, String> {

    Cliente findClienteById(String id);

    @EntityGraph(attributePaths = {"cliente"})
    Optional<Persona> findById(String id);
}
