package com.neoris.ejecicioneoris.infrastructure.repositories;

import com.neoris.ejecicioneoris.domain.entities.Movimiento;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface MovimientoRepositoryInterface extends JpaRepository<Movimiento,Integer> {
    @EntityGraph(attributePaths = {"cliente", "cuenta"})
    Optional<Movimiento> findById(Integer id);

    Optional<List<Movimiento>> findByFechaBetweenAndCuentaNumeroCuentaOrderByFechaAsc(LocalDate fechaInicio, LocalDate fechaFin, String numeroCuenta);
}
