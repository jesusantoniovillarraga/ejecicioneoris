package com.neoris.ejecicioneoris.domain.handlers;

import com.neoris.ejecicioneoris.domain.errors.ApiError;
import com.neoris.ejecicioneoris.domain.errors.ApiSubError;
import com.neoris.ejecicioneoris.domain.errors.ApiValidationError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.LinkedList;
import java.util.List;

@RestControllerAdvice
public class ValidationExceptionHandler {
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ApiError handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        List<ApiSubError> errors = new LinkedList<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String object = error.getObjectName();
            String fieldName = ((FieldError) error).getField();
            Object rejectedValue = ((FieldError) error).getRejectedValue();
            String errorMessage = error.getDefaultMessage();
            ApiSubError subError = new ApiValidationError(object,fieldName,rejectedValue,errorMessage);
            errors.add(subError);
        });
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST,"Validation errors", ex);
        apiError.setSubErrors(errors);
        return apiError;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({HttpMessageNotReadableException.class})
    protected ApiError handleHttpMessageNotReadable(HttpMessageNotReadableException ex) {
        String error = "Malformed JSON request";
        return new ApiError(HttpStatus.BAD_REQUEST, error, ex);
    }

    private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }

}
