package com.neoris.ejecicioneoris.domain.entities;

import com.neoris.ejecicioneoris.domain.enums.Genero;
import jakarta.annotation.Nullable;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Persona {

    @Id
    @Column(length = 15)
    private String id;

    @NotNull
    @Pattern(regexp = "([A-Za-z0-9\\s-_]+)")
    @Column(length = 30)
    private String nombre;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Genero genero;

    @NotNull
    private Integer edad;

    @Nullable
    @Pattern(regexp = "([A-Za-z0-9\\s-_]+)")
    @Column(length = 50)
    private String direccion;

    @Nullable
    @Pattern(regexp = "([0-9\\s-_]+)")
    @Column(length = 15)
    private String telefono;

    @OneToOne(mappedBy = "persona")
    private Cliente cliente;
}
