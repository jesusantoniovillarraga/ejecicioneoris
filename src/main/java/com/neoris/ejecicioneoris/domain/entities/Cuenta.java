package com.neoris.ejecicioneoris.domain.entities;

import com.neoris.ejecicioneoris.domain.enums.Estado;
import com.neoris.ejecicioneoris.domain.enums.TipoCuenta;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
public class Cuenta {

    @Id
    @Column(length = 18)
    private String numeroCuenta;

    @Enumerated(EnumType.STRING)
    @NotNull
    private TipoCuenta tipoCuenta;

    @NotNull
    private Integer saldoInicial;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Estado estado;

    @ManyToOne
    @JoinColumn(name = "cliente_id")
    private Cliente cliente;

    @OneToMany(mappedBy="cuenta")
    private List<Movimiento> movimientos;
}
