package com.neoris.ejecicioneoris.domain.entities;

import com.neoris.ejecicioneoris.domain.enums.TipoMovimiento;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Movimiento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer movmientoId;

    @Enumerated(EnumType.STRING)
    @NotNull
    private TipoMovimiento tipoMovimiento;

    @NotNull
    private Integer valor;

    @NotNull
    private Integer saldo;

    @NotNull
    private LocalDate fecha;

    @ManyToOne
    @JoinColumn(name="cuenta_id", nullable=false)
    private Cuenta cuenta;

    @ManyToOne
    @JoinColumn(name = "cliente_id", nullable=false)
    private Cliente cliente;
}
