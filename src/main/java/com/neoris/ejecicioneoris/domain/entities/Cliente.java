package com.neoris.ejecicioneoris.domain.entities;

import com.neoris.ejecicioneoris.domain.enums.Estado;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer clienteId;

    @NotNull
    @Size(min = 8)
    private String contrasenia;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Estado estado;

    @OneToOne
    @JoinColumn(name = "persona_id")
    private Persona persona;

    @OneToMany(mappedBy = "cliente")
    private List<Cuenta> cuentas;

    @OneToMany(mappedBy="cliente")
    private List<Movimiento> movimientos;
}
