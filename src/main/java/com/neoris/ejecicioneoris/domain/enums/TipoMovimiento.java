package com.neoris.ejecicioneoris.domain.enums;

public enum TipoMovimiento {
    DEBITO,
    CREDITO
}
