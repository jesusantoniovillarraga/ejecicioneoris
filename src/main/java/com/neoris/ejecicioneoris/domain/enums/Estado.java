package com.neoris.ejecicioneoris.domain.enums;

public enum Estado {
    ACTIVO,
    INACTIVO
}
