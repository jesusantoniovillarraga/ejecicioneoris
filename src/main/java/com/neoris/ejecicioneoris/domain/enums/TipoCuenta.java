package com.neoris.ejecicioneoris.domain.enums;

public enum TipoCuenta {
    AHORRO,
    CORRIENTE
}
