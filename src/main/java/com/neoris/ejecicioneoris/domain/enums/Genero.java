package com.neoris.ejecicioneoris.domain.enums;

public enum Genero {
    MASCULINO,
    FEMENINO
}
