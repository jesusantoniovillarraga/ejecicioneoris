package com.neoris.ejecicioneoris;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjecicioNeorisApplication {

    public static void main(String[] args) {
        SpringApplication.run(EjecicioNeorisApplication.class, args);
    }

}
