package com.neoris.ejecicioneoris.application.dto.autenticacion;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CambiarContraseniaDto {

    @JsonProperty("correo")
    @Size(min = 0, max = 255)
    @NotNull
    @Email
    @Schema(
            description = "Correo del usuario existente",
            type = "string",
            example = "newuser01@gmail.com"
    )
    private String correo;

    @JsonProperty("contrasena")
    @Size(min = 0, max = 255)
    @NotNull
    @Schema(
            description = "Conraseña del usuario existente",
            type = "string",
            example = "securePassword1!"
    )
    private String contrasenia;

    @JsonProperty("confirmacion_contrasena")
    @Size(min = 0, max = 255)
    @NotNull
    @Schema(
            description = "Confirmacion de la contraseña",
            type = "string",
            example = "securePassword1!"
    )
    private String confirmacionContrasenia;
}
