package com.neoris.ejecicioneoris.application.dto.autenticacion;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IniciarSesionDto {

    @JsonProperty("identificacion")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Numero de identificacion de la persona dueña de la cuenta",
            type = "string",
            example = "1110111111"
    )
    private String username;

    @JsonProperty("contrasenia")
    @Size(min = 0, max = 255)
    @NotNull
    @Schema(
            description = "Contraseña del usuario",
            type = "string",
            example = "securePassword1!"
    )
    private String password;
}
