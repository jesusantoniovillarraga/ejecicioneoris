package com.neoris.ejecicioneoris.application.dto.autenticacion;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteAutenticadoDto {

    @JsonProperty("tipo_token")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Tipo de token",
            type = "string"
    )
    private String tipoToken = "Bearer ";

    @JsonProperty("token")
    @NotNull
    @Schema(
            description = "Token valido para el usuario",
            type = "string"
    )
    private String token;

    public ClienteAutenticadoDto(String token) {
        this.token = token;
    }
}
