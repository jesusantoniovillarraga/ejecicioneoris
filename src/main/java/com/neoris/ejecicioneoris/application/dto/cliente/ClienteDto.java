package com.neoris.ejecicioneoris.application.dto.cliente;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neoris.ejecicioneoris.application.dto.cuenta.CuentaDto;
import com.neoris.ejecicioneoris.application.dto.persona.PersonaDto;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ClienteDto {

    @JsonProperty("persona")
    @Schema(
            description = "Informacion personal de la cuenta cliente",
            type = "Persona"
    )
    private PersonaDto persona;

    @JsonProperty("estado")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Estado de la cuenta cliente",
            type = "string",
            example = "ACTIVO"
    )
    private String estado;

    @JsonProperty("contrasenia")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Contrasenia de la cuenta cliente",
            type = "string",
            example = "securepassword01"
    )
    private String contrasenia;

    @JsonProperty("cuentas")
    @Schema(
            description = "Cuentas creadas para el usuario",
            type = "object array"
    )
    List<@Valid CuentaDto> cuentas;
}
