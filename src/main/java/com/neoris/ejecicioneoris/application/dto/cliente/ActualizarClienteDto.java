package com.neoris.ejecicioneoris.application.dto.cliente;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActualizarClienteDto {

    @JsonProperty("estado")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Estado de la cuenta cliente",
            type = "string",
            example = "ACTIVO"
    )
    private String estado;
}
