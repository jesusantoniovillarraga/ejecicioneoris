package com.neoris.ejecicioneoris.application.dto.movmiento;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MovimientoDto {

    @JsonProperty("tipo_movmiento")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Tipo del movimiento que se va a realizar",
            type = "string",
            example = "CREDITO"
    )
    private String tipoMovimiento;

    @JsonProperty("valor")
    @NotNull
    @Schema(
            description = "Valor del movmiento a realizar",
            type = "Integer",
            example = "12000"
    )
    private Integer valor;

    @JsonProperty("numero_cuenta")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Numero de cuenta sobre la cual se va a realizar el movimiento",
            type = "string",
            example = "9099581745"
    )
    private String numeroCuenta;
}
