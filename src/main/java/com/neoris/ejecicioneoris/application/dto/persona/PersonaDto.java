package com.neoris.ejecicioneoris.application.dto.persona;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neoris.ejecicioneoris.application.dto.cliente.ClienteDto;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonaDto {

    @JsonProperty("identificacion")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Identificacion personal",
            type = "string",
            example = "9099581745"
    )
    private String identificacion;

    @JsonProperty("nombre")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Nombre de la persona",
            type = "string",
            example = "Pepito Perez"
    )
    private String nombre;

    @JsonProperty("genero")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Genero de la persona",
            type = "string",
            example = "MASCULINO"
    )
    private String genero;

    @JsonProperty("direccion")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Direccion de la persona",
            type = "string",
            example = "9099581745679890"
    )
    private String direccion;

    @JsonProperty("telefono")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Telefono de la persona",
            type = "string",
            example = "9099581745679890"
    )
    private String telefono;

    @JsonProperty("edad")
    @NotNull
    @Schema(
            description = "Edad de la persona",
            type = "Integer",
            example = "20"
    )
    private String edad;
}
