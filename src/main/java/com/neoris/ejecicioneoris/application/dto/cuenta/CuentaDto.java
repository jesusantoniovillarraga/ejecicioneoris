package com.neoris.ejecicioneoris.application.dto.cuenta;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CuentaDto {

    @JsonProperty("numero_cuenta")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Numero de cuenta",
            type = "string",
            example = "9099581745679890"
    )
    private String numeroCuenta;

    @JsonProperty("tipo_cuenta")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Tipo de la cuenta",
            type = "string",
            example = "AHORRO"
    )
    private String tipoCuenta;

    @JsonProperty("saldo_inicial")
    @NotNull
    @Schema(
            description = "Saldo inicial de la cuenta",
            type = "Integer",
            example = "100"
    )
    private Integer saldoInicial;

    @JsonProperty("identificacion")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Numero de intetificacion del cliente al cual se le va  asociar la cuenta",
            type = "string",
            example = "9099581745679890"
    )
    private String identificacion;
}
