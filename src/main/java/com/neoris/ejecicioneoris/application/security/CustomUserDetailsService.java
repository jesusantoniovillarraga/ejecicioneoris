package com.neoris.ejecicioneoris.application.security;

import com.neoris.ejecicioneoris.domain.entities.Cliente;
import com.neoris.ejecicioneoris.infrastructure.repositories.ClienteRepositoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private ClienteRepositoryInterface repository;

    @Override
    public UserDetails loadUserByUsername(String identificacion) throws UsernameNotFoundException {
        List<String> roles = new LinkedList<>();
        roles.add("USER");
        Cliente user = repository.findClienteByPersonaId(identificacion).orElseThrow(()-> new UsernameNotFoundException("Cliente no encontrado"));
        return new User(user.getPersona().getId(),user.getContrasenia(), mapRoles(roles));
    }

    private Collection<GrantedAuthority> mapRoles(List<String> roles){
        return roles.stream().map(role->new SimpleGrantedAuthority(role)).collect(Collectors.toList());
    }
}
