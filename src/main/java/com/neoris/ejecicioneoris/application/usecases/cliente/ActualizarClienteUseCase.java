package com.neoris.ejecicioneoris.application.usecases.cliente;

import com.neoris.ejecicioneoris.application.services.ClienteService;
import com.neoris.ejecicioneoris.application.usecases.contracts.cliente.ActualizarClienteUseCaseInterface;
import com.neoris.ejecicioneoris.domain.enums.Estado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ActualizarClienteUseCase implements ActualizarClienteUseCaseInterface {

    @Autowired
    private ClienteService clienteService;

    @Override
    public void actualizarCliente(String id, String estado) {
        clienteService.cambiarEstadoCliente(id, Estado.valueOf(estado));
    }
}
