package com.neoris.ejecicioneoris.application.usecases.cliente;

import com.neoris.ejecicioneoris.application.dto.cliente.ClienteDto;
import com.neoris.ejecicioneoris.application.services.ClienteService;
import com.neoris.ejecicioneoris.application.usecases.contracts.cliente.ObtenerClienteUseCaseInterface;
import com.neoris.ejecicioneoris.infrastructure.mappers.ClienteMapperInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ObtenerClienteUseCase implements ObtenerClienteUseCaseInterface {
    @Autowired
    private ClienteService clienteService;

    @Override
    public ClienteDto obtenerCliente(String personaId) {
        return ClienteMapperInterface.MAPPER.fromCliente(clienteService.obtenerClientePorPersonaId(personaId));
    }
}
