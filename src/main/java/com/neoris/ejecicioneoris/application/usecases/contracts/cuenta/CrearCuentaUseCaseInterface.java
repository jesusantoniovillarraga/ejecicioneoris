package com.neoris.ejecicioneoris.application.usecases.contracts.cuenta;

import com.neoris.ejecicioneoris.application.dto.cuenta.CuentaDto;

public interface CrearCuentaUseCaseInterface {

    public void crearCuenta(CuentaDto cuentaDto);
}
