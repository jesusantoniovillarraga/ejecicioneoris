package com.neoris.ejecicioneoris.application.usecases.contracts.cuenta;

import com.neoris.ejecicioneoris.application.dto.movmiento.MovimientoDto;

public interface ActualizarCuentaUseCaseInterface {

    public void actualizarCuenta(String numeroCuenta, MovimientoDto movimientoDto) throws Exception;
}
