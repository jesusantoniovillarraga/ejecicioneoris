package com.neoris.ejecicioneoris.application.usecases.cliente;

import com.neoris.ejecicioneoris.application.services.ClienteService;
import com.neoris.ejecicioneoris.application.usecases.contracts.cliente.EliminarClienteUseCaseInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EliminarClienteUseCase implements EliminarClienteUseCaseInterface {

    @Autowired
    private ClienteService clienteService;

    @Override
    public void eliminarCliente(String personaId) {
        clienteService.deleteCliente(personaId);
    }
}
