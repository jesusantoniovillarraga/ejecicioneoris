package com.neoris.ejecicioneoris.application.usecases.cliente;

import com.neoris.ejecicioneoris.application.dto.cliente.ClienteDto;
import com.neoris.ejecicioneoris.application.services.ClienteService;
import com.neoris.ejecicioneoris.application.services.PersonaService;
import com.neoris.ejecicioneoris.application.usecases.contracts.cliente.CrearClienteUseCaseInterface;
import com.neoris.ejecicioneoris.domain.entities.Cliente;
import com.neoris.ejecicioneoris.infrastructure.mappers.ClienteMapperInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CrearClienteUseCase implements CrearClienteUseCaseInterface {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private PersonaService personaService;

    @Override
    public void crearCliente(ClienteDto clienteDto) {
        Cliente cliente = ClienteMapperInterface.MAPPER.toCliente(clienteDto);

        personaService.crearPersona(cliente.getPersona());
        clienteService.crearCliente(cliente);
    }
}
