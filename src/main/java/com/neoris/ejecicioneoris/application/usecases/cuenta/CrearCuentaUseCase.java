package com.neoris.ejecicioneoris.application.usecases.cuenta;

import com.neoris.ejecicioneoris.application.dto.cuenta.CuentaDto;
import com.neoris.ejecicioneoris.application.services.ClienteService;
import com.neoris.ejecicioneoris.application.services.CuentaService;
import com.neoris.ejecicioneoris.application.usecases.contracts.cuenta.CrearCuentaUseCaseInterface;
import com.neoris.ejecicioneoris.domain.entities.Cliente;
import com.neoris.ejecicioneoris.domain.entities.Cuenta;
import com.neoris.ejecicioneoris.domain.enums.Estado;
import com.neoris.ejecicioneoris.infrastructure.mappers.CuentaMapperInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CrearCuentaUseCase implements CrearCuentaUseCaseInterface {

    @Autowired
    private CuentaService cuentaService;

    @Autowired
    private ClienteService clienteService;

    @Override
    public void crearCuenta(CuentaDto cuentaDto) {
        Cuenta cuenta = CuentaMapperInterface.MAPPER.toCuenta(cuentaDto);
        cuenta.setEstado(Estado.ACTIVO);
        Cliente cliente = clienteService.obtenerClientePorPersonaId(cuentaDto.getIdentificacion());
        cuenta.setCliente(cliente);
        cliente.getCuentas().add(cuenta);
        cuentaService.crearCuenta(cuenta);
    }
}
