package com.neoris.ejecicioneoris.application.usecases.movimiento;

import com.neoris.ejecicioneoris.application.dto.movmiento.MovimientoDto;
import com.neoris.ejecicioneoris.application.services.CuentaService;
import com.neoris.ejecicioneoris.application.services.MovimientoService;
import com.neoris.ejecicioneoris.application.usecases.contracts.movimiento.CrearMovimientoUseCaseInterface;
import com.neoris.ejecicioneoris.domain.entities.Cuenta;
import com.neoris.ejecicioneoris.domain.entities.Movimiento;
import com.neoris.ejecicioneoris.infrastructure.mappers.MovimientoMapperInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class CrearMovimientoUseCase implements CrearMovimientoUseCaseInterface {

    @Autowired
    private MovimientoService movimientoService;

    @Autowired
    private CuentaService cuentaService;

    @Override
    public void crearMovimiento(MovimientoDto movimientoDto) {
        Movimiento movimiento = MovimientoMapperInterface.MAPPER.toMovimiento(movimientoDto);
        Cuenta cuenta = cuentaService.obtenerCuentaPorNumeroCuenta(movimientoDto.getNumeroCuenta());
        movimiento.setCuenta(cuenta);
        movimiento.setCliente(cuenta.getCliente());
        movimiento.setFecha(LocalDate.now());
        movimiento.setSaldo(cuenta.getSaldoInicial());
        movimientoService.crearMovimiento(movimiento);
    }
}
