package com.neoris.ejecicioneoris.application.usecases.contracts.movimiento;

import com.neoris.ejecicioneoris.application.dto.movmiento.MovimientoDto;

public interface CrearMovimientoUseCaseInterface {
    public void crearMovimiento(MovimientoDto movimientoDto);
}
