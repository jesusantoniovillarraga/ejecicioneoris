package com.neoris.ejecicioneoris.application.usecases.cuenta;

import com.neoris.ejecicioneoris.application.dto.cuenta.CuentaDto;
import com.neoris.ejecicioneoris.application.services.CuentaService;
import com.neoris.ejecicioneoris.application.usecases.contracts.cuenta.ObtenerCuentaUseCaseInterface;
import com.neoris.ejecicioneoris.infrastructure.mappers.CuentaMapperInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ObtenerCuentaUseCase implements ObtenerCuentaUseCaseInterface {

    @Autowired
    private CuentaService cuentaService;

    @Override
    public CuentaDto obtenerCuenta(String numeroCuenta) {
        return CuentaMapperInterface.MAPPER.fromCuenta(cuentaService.obtenerCuentaPorNumeroCuenta(numeroCuenta));
    }
}
