package com.neoris.ejecicioneoris.application.usecases.movimiento;

import com.neoris.ejecicioneoris.application.dto.movmiento.MovimientoDto;
import com.neoris.ejecicioneoris.application.services.MovimientoService;
import com.neoris.ejecicioneoris.application.usecases.contracts.movimiento.ObtenerMovimientoUseCaseInterface;
import com.neoris.ejecicioneoris.infrastructure.mappers.MovimientoMapperInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ObtenerMovimientoUseCase implements ObtenerMovimientoUseCaseInterface {

    @Autowired
    private MovimientoService movimientoService;

    @Override
    public MovimientoDto obtenerMovimeintoPorId(Integer id) {
        return MovimientoMapperInterface.MAPPER.fromMovimiento(movimientoService.obtenerMovmientoPorId(id));
    }

    @Override
    public List<MovimientoDto> obtenerMovimientoEntreFechas(LocalDate fechaInicio, LocalDate fechaFin, String numeroCuenta) {
        return movimientoService.obtenerMovmientosEntreFechas(fechaInicio,fechaFin,numeroCuenta)
                .stream()
                .map(MovimientoMapperInterface.MAPPER::fromMovimiento)
                .collect(Collectors.toList());
    }
}
