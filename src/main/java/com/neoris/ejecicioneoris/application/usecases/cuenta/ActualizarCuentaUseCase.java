package com.neoris.ejecicioneoris.application.usecases.cuenta;

import com.neoris.ejecicioneoris.application.dto.movmiento.MovimientoDto;
import com.neoris.ejecicioneoris.application.services.CuentaService;
import com.neoris.ejecicioneoris.application.usecases.contracts.cuenta.ActualizarCuentaUseCaseInterface;
import com.neoris.ejecicioneoris.domain.entities.Cuenta;
import com.neoris.ejecicioneoris.domain.entities.Movimiento;
import com.neoris.ejecicioneoris.infrastructure.mappers.MovimientoMapperInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ActualizarCuentaUseCase implements ActualizarCuentaUseCaseInterface {

    @Autowired
    private CuentaService cuentaService;

    @Override
    public void actualizarCuenta(String numeroCuenta, MovimientoDto movimientoDto) throws Exception {
        Cuenta cuenta = cuentaService.obtenerCuentaPorNumeroCuenta(numeroCuenta);
        Movimiento movimiento = MovimientoMapperInterface.MAPPER.toMovimiento(movimientoDto);
        switch(movimiento.getTipoMovimiento()){
            case DEBITO:
                if(movimiento.getValor() > cuenta.getSaldoInicial()){
                    throw new Exception("Saldo no disponible");
                }
                Integer saldoDebito = cuenta.getSaldoInicial() - movimiento.getValor();
                cuentaService.actualizarSaldo(numeroCuenta, saldoDebito);
                break;
            case CREDITO:
                Integer saldoCredito = cuenta.getSaldoInicial() + movimiento.getValor();
                cuentaService.actualizarSaldo(numeroCuenta, saldoCredito);
        }
    }
}
