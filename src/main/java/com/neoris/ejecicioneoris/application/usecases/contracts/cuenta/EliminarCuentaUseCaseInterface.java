package com.neoris.ejecicioneoris.application.usecases.contracts.cuenta;


public interface EliminarCuentaUseCaseInterface {
    public void eliminarCuenta(String numeroCuenta);
}
