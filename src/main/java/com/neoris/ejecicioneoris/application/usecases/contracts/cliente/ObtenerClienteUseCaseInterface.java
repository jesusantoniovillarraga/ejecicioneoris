package com.neoris.ejecicioneoris.application.usecases.contracts.cliente;

import com.neoris.ejecicioneoris.application.dto.cliente.ClienteDto;
import org.springframework.stereotype.Component;

public interface ObtenerClienteUseCaseInterface {
    public ClienteDto obtenerCliente(String personaId);
}
