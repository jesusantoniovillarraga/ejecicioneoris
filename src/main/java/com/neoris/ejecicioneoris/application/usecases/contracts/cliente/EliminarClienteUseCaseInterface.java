package com.neoris.ejecicioneoris.application.usecases.contracts.cliente;

import org.springframework.stereotype.Component;

public interface EliminarClienteUseCaseInterface {
    public void eliminarCliente(String personaId);
}
