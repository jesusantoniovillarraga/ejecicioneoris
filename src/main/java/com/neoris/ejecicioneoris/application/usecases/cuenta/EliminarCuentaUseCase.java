package com.neoris.ejecicioneoris.application.usecases.cuenta;

import com.neoris.ejecicioneoris.application.services.CuentaService;
import com.neoris.ejecicioneoris.application.usecases.contracts.cuenta.EliminarCuentaUseCaseInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EliminarCuentaUseCase implements EliminarCuentaUseCaseInterface {

    @Autowired
    private CuentaService cuentaService;

    @Override
    public void eliminarCuenta(String numeroCuenta) {
        cuentaService.eliminarCuenta(numeroCuenta);
    }
}
