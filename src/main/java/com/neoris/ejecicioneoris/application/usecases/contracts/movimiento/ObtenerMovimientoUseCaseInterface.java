package com.neoris.ejecicioneoris.application.usecases.contracts.movimiento;

import com.neoris.ejecicioneoris.application.dto.movmiento.MovimientoDto;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface ObtenerMovimientoUseCaseInterface {
    public MovimientoDto obtenerMovimeintoPorId(Integer id);

    public List<MovimientoDto> obtenerMovimientoEntreFechas(LocalDate fechaInicio, LocalDate fechaFin, String numeroCuenta);
}
