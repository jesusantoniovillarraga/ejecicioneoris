package com.neoris.ejecicioneoris.application.controllers;

import com.neoris.ejecicioneoris.application.dto.cliente.ActualizarClienteDto;
import com.neoris.ejecicioneoris.application.dto.cliente.ClienteDto;
import com.neoris.ejecicioneoris.application.usecases.contracts.cliente.ActualizarClienteUseCaseInterface;
import com.neoris.ejecicioneoris.application.usecases.contracts.cliente.CrearClienteUseCaseInterface;
import com.neoris.ejecicioneoris.application.usecases.contracts.cliente.EliminarClienteUseCaseInterface;
import com.neoris.ejecicioneoris.application.usecases.contracts.cliente.ObtenerClienteUseCaseInterface;
import com.neoris.ejecicioneoris.domain.errors.ApiError;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/clientes")
public class ClienteController {

    @Autowired
    private ActualizarClienteUseCaseInterface actualizarUseCase;

    @Autowired
    private CrearClienteUseCaseInterface crearUseCase;

    @Autowired
    private EliminarClienteUseCaseInterface eliminarUseCase;

    @Autowired
    private ObtenerClienteUseCaseInterface obtenerUseCase;

    @PostMapping("")
    @Operation(summary = "Crea un nuevo cliente", description = "Crea un nuevo cliente", tags = { "Clientes" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Operacion exitosa", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ClienteDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Peticion equivocada", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity<String> crearCliente(
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Informacion para registrar un nuevo cliente",
            required = true) @Valid @RequestBody ClienteDto clienteDto){
        crearUseCase.crearCliente(clienteDto);
        return new ResponseEntity<>("Cliente creado exitosamente", HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Elimina un cliente dado la identificacion de la persona",
            description = "Elimina un cliente dado la identificacion de la persona", tags = { "Clientes" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Operacion exitosa", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = String.class)) }),
            @ApiResponse(responseCode = "400", description = "Peticion equivocada", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity<String> eliminarCliente(@Parameter(description = "La identificacion de la persona registrada",
            required = true) @PathVariable String id){
        eliminarUseCase.eliminarCliente(id);
        return new ResponseEntity<>("Cliente eliminado correctamente", HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Obtiene la informacion y la informacion personal de un cliente",
            description = "Obtiene la informacion y la informacion personal de un cliente", tags = { "Clientes" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Operacion exitosa", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ClienteDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Peticion equivocada", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity<ClienteDto> obtenerCliente(@Parameter(description = "La identificacion de la persona registrada",
            required = true) @PathVariable String id){
        ClienteDto result = obtenerUseCase.obtenerCliente(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    @Operation(summary = "Actualiza el estado de un cliente dado la identificacion de la persona",
            description = "Actualiza el estado de un cliente dado la identificacion de la persona", tags = { "Clientes" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Operacion exitosa", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = String.class)) }),
            @ApiResponse(responseCode = "400", description = "Peticion equivocada", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity<String> actualizarCliente(@Parameter(description = "La identificacion de la persona registrada", required = true)
                                              @PathVariable String id,
                                              @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "El nuevo estado para la cuenta cliente",
                                              required = true) @Valid @RequestBody ActualizarClienteDto actualizarClienteDto){
        actualizarUseCase.actualizarCliente(id,actualizarClienteDto.getEstado());
        return new ResponseEntity<>("Estado del cliente actualizado correctamente", HttpStatus.OK);
    }
}
