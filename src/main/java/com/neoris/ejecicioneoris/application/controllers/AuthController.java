package com.neoris.ejecicioneoris.application.controllers;

import com.neoris.ejecicioneoris.application.dto.autenticacion.ClienteAutenticadoDto;
import com.neoris.ejecicioneoris.application.dto.autenticacion.IniciarSesionDto;
import com.neoris.ejecicioneoris.application.dto.cliente.ClienteDto;
import com.neoris.ejecicioneoris.application.security.JWTGenerator;
import com.neoris.ejecicioneoris.application.usecases.contracts.cliente.CrearClienteUseCaseInterface;
import com.neoris.ejecicioneoris.application.usecases.contracts.cliente.ObtenerClienteUseCaseInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    private final AuthenticationManager authenticationManager;
    private CrearClienteUseCaseInterface crearUseCase;
    private ObtenerClienteUseCaseInterface obtenerClienteUseCase;
    private final PasswordEncoder passwordEncoder;
    private JWTGenerator jwtGenerator;

    @Autowired
    public AuthController(AuthenticationManager authenticationManager,
                          CrearClienteUseCaseInterface crearUseCase,
                          ObtenerClienteUseCaseInterface obtenerClienteUseCase,
                          PasswordEncoder passwordEncoder,JWTGenerator jwtGenerator) {
        this.authenticationManager = authenticationManager;
        this.crearUseCase = crearUseCase;
        this.obtenerClienteUseCase = obtenerClienteUseCase;
        this.passwordEncoder = passwordEncoder;
        this.jwtGenerator = jwtGenerator;
    }

    @PostMapping("register")
    public ResponseEntity<String> register(@RequestBody ClienteDto registerDto){
        if(obtenerClienteUseCase.obtenerCliente(registerDto.getPersona().getIdentificacion()) != null){
            return new ResponseEntity<>("El usuario ya ha sido creado", HttpStatus.BAD_REQUEST);
        }
        registerDto.setContrasenia(passwordEncoder.encode((registerDto.getContrasenia())));

        crearUseCase.crearCliente(registerDto);
        return new ResponseEntity<>("User registration success", HttpStatus.OK);
    }

    @PostMapping("login")
    public ResponseEntity<ClienteAutenticadoDto> login(@RequestBody IniciarSesionDto loginDto){
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginDto.getUsername(),
                        loginDto.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtGenerator.generateToken(authentication);
        return new ResponseEntity<>(new ClienteAutenticadoDto(token), HttpStatus.OK);
    }
}
