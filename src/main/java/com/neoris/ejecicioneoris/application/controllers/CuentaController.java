package com.neoris.ejecicioneoris.application.controllers;

import com.neoris.ejecicioneoris.application.dto.cliente.ClienteDto;
import com.neoris.ejecicioneoris.application.dto.cuenta.CuentaDto;
import com.neoris.ejecicioneoris.application.usecases.contracts.cuenta.CrearCuentaUseCaseInterface;
import com.neoris.ejecicioneoris.application.usecases.contracts.cuenta.EliminarCuentaUseCaseInterface;
import com.neoris.ejecicioneoris.application.usecases.contracts.cuenta.ObtenerCuentaUseCaseInterface;
import com.neoris.ejecicioneoris.domain.errors.ApiError;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/cuentas")
public class CuentaController {

    @Autowired
    private CrearCuentaUseCaseInterface crearCuentaUseCase;

    @Autowired
    private EliminarCuentaUseCaseInterface eliminarCuentaUseCase;

    @Autowired
    private ObtenerCuentaUseCaseInterface obtenerCuentaUseCase;

    @PostMapping("")
    @Operation(summary = "Crea una nueva cuenta", description = "Crea una nueva cuenta", tags = { "Cuentas" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Operacion exitosa", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CuentaDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Peticion equivocada", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity<String> crearCuenta(
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Informacion para registrar una nueva cuenta",
                    required = true) @Valid @RequestBody CuentaDto cuentaDto){
        crearCuentaUseCase.crearCuenta(cuentaDto);
        return new ResponseEntity<>("Cuenta creada exitosamente", HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Elimina una cuenta dado el numero de cuenta",
            description = "Elimina una cuenta dado el numero de cuenta", tags = { "Cuentas" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Operacion exitosa", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = String.class)) }),
            @ApiResponse(responseCode = "400", description = "Peticion equivocada", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity<String> eliminarCuenta(@Parameter(description = "El numero de la cuenta",
            required = true) @PathVariable String id){
        eliminarCuentaUseCase.eliminarCuenta(id);
        return new ResponseEntity<>("Cuenta eliminada correctamente", HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Obtiene la informacion de una cuenta junto a sus movimiento",
            description = "Obtiene la informacion de una cuenta junto a sus movimiento", tags = { "Cuentas" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Operacion exitosa", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CuentaDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Peticion equivocada", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity<CuentaDto> obtenerCuenta(@Parameter(description = "El numero de la cuenta registrada",
            required = true) @PathVariable String id){
        CuentaDto result = obtenerCuentaUseCase.obtenerCuenta(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
