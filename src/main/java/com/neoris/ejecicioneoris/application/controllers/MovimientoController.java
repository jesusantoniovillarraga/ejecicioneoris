package com.neoris.ejecicioneoris.application.controllers;

import com.neoris.ejecicioneoris.application.dto.movmiento.MovimientoDto;
import com.neoris.ejecicioneoris.application.usecases.contracts.cuenta.ActualizarCuentaUseCaseInterface;
import com.neoris.ejecicioneoris.application.usecases.contracts.movimiento.CrearMovimientoUseCaseInterface;
import com.neoris.ejecicioneoris.application.usecases.contracts.movimiento.ObtenerMovimientoUseCaseInterface;
import com.neoris.ejecicioneoris.domain.errors.ApiError;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("api/movimientos")
public class MovimientoController {

    @Autowired
    private CrearMovimientoUseCaseInterface crearMovimientoUseCase;

    @Autowired
    private ActualizarCuentaUseCaseInterface actualizarCuentaUseCase;

    @Autowired
    private ObtenerMovimientoUseCaseInterface obtenerMovimientoUseCase;

    @PostMapping("")
    @Operation(summary = "Crea un nuevo movimiento", description = "Crea un nuevo movimiento", tags = { "Movimientos" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Operacion exitosa", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovimientoDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Peticion equivocada", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    @Transactional
    public ResponseEntity crearMovimiento(
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Informacion para registrar unnuevo movimeinto",
                    required = true) @Valid @RequestBody MovimientoDto movimientoDto){
        try{
            actualizarCuentaUseCase.actualizarCuenta(movimientoDto.getNumeroCuenta(),movimientoDto);
            crearMovimientoUseCase.crearMovimiento(movimientoDto);
            return new ResponseEntity<>("Movmiento creado exitosamente", HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(
                    new ApiError(HttpStatus.BAD_REQUEST,
                            e.getMessage(),
                            e
                    ), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{numeroCuenta}")
    @Operation(summary = "Obtiene la informacion de los movimientos realizado entre fechas",
            description = "Obtiene la informacion de los movimientos realizado entre fechas", tags = { "Movimientos" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Operacion exitosa", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovimientoDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Peticion equivocada", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity<List<MovimientoDto>> obtenerMovimimentosEntreFechas(@Parameter(description = "El numero de cuenta",
            required = true) @PathVariable String numeroCuenta,
           @Parameter(description = "La fecha de inicio de la busqueda", required = true) @RequestParam("fechaInicio") LocalDate fechaInicio,
           @Parameter(description = "La fecha de fin de la busqueda", required = true) @RequestParam("fechaFin") LocalDate fechaFin){
        List<MovimientoDto> result = obtenerMovimientoUseCase.obtenerMovimientoEntreFechas(fechaInicio,fechaFin,numeroCuenta);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
