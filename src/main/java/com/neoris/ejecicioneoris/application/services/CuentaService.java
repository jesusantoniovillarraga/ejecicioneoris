package com.neoris.ejecicioneoris.application.services;

import com.neoris.ejecicioneoris.domain.entities.Cuenta;
import com.neoris.ejecicioneoris.infrastructure.repositories.CuentaRepositoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CuentaService {

    @Autowired
    private CuentaRepositoryInterface repository;

    public Cuenta obtenerCuentaPorNumeroCuenta(String numeroCuenta){
        return repository.findById(numeroCuenta).orElse(null);
    }

    public void crearCuenta(Cuenta cuenta){
        repository.save(cuenta);
    }

    public void actualizarSaldo(String id, Integer saldoActual){
        Cuenta cuenta = this.obtenerCuentaPorNumeroCuenta(id);
        cuenta.setSaldoInicial(saldoActual);
        repository.save(cuenta);
    }

    public void eliminarCuenta(String numeroCuenta){
        Cuenta cuenta = this.obtenerCuentaPorNumeroCuenta(numeroCuenta);
        repository.delete(cuenta);
    }
}
