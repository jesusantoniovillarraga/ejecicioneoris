package com.neoris.ejecicioneoris.application.services;

import com.neoris.ejecicioneoris.domain.entities.Persona;
import com.neoris.ejecicioneoris.infrastructure.repositories.PersonaRepositoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonaService {

    @Autowired
    private PersonaRepositoryInterface repository;

    public Persona obtenerPersonaPorId(String id){
        return repository.findById(id).orElse(null);
    }

    public void crearPersona(Persona persona){
        repository.save(persona);
    }

    public void actualizarPersona(String id, Persona persona){
        Persona personaActualizar = this.obtenerPersonaPorId(id);
        personaActualizar.setEdad(persona.getEdad());
        personaActualizar.setDireccion(persona.getDireccion());
        personaActualizar.setTelefono(persona.getTelefono());
        personaActualizar.setNombre(persona.getNombre());
        personaActualizar.setGenero(persona.getGenero());
        repository.save(personaActualizar);
    }
}
