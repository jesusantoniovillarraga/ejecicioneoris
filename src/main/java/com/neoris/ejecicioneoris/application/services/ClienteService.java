package com.neoris.ejecicioneoris.application.services;

import com.neoris.ejecicioneoris.domain.entities.Cliente;
import com.neoris.ejecicioneoris.domain.enums.Estado;
import com.neoris.ejecicioneoris.infrastructure.repositories.ClienteRepositoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepositoryInterface repository;

    public Cliente obtenerClientePorPersonaId(String id){
        return repository.findClienteByPersonaId(id).orElse(null);
    }

    public void crearCliente(Cliente cliente){
        repository.save(cliente);
    }

    public void cambiarEstadoCliente(String id, Estado estado){
        Cliente cliente = this.obtenerClientePorPersonaId(id);
        cliente.setEstado(estado);
        repository.save(cliente);
    }

    public void deleteCliente(String id){
        Cliente cliente = this.obtenerClientePorPersonaId(id);
        repository.delete(cliente);
    }
}
