package com.neoris.ejecicioneoris.application.services;

import com.neoris.ejecicioneoris.domain.entities.Movimiento;
import com.neoris.ejecicioneoris.infrastructure.repositories.MovimientoRepositoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Service
public class MovimientoService {

    @Autowired
    private MovimientoRepositoryInterface repository;

    public Movimiento obtenerMovmientoPorId(Integer id){
        return repository.findById(id).orElse(null);
    }

    public List<Movimiento> obtenerMovmientosEntreFechas(LocalDate fechaInicio, LocalDate fechaFin, String numeroCuenta){
        return repository.findByFechaBetweenAndCuentaNumeroCuentaOrderByFechaAsc(fechaInicio, fechaFin, numeroCuenta).orElse(null);
    }

    public void crearMovimiento(Movimiento movimiento){
        repository.save(movimiento);
    }
}
